# Commands

after git pull or clone:
`yarn install` / `npm install`

run server:
`yarn serve` / `npm run serve`

# Structures

- `assets`: CSS, Images
- `components`: Reusable components or parts of view, such as menu or sidebar.
- `const`: API constants. Deleted soon, moving to `store` for easier code.
- `mixins`: Reusable Vue.js code.
- `router`: As its name, router files.
- `store`: Vuex store, used for state management and API calls.
- `utils`: Utility functions.
- `views`: Views.
