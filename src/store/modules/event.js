import axios from "axios";

const actions = {
  async GET(_context, params) {
    const { data } = await axios.get("/event", { params });

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async SHOW(_context, id) {
    const { data } = await axios.get(`/event/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async STORE(_context, values) {
    const { data } = await axios.post(`/event`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPDATE(_context, values) {
    const { data } = await axios.put(`/event/${values.id}`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DESTROY(_context, id) {
    const { data } = await axios.delete(`/event/${id}`);

    if (!data.success) {
      throw data.message;
    }
  }
};

export default {
  namespaced: true,
  actions
};
