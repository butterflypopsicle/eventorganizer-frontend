import Axios from "axios";

const actions = {
  async GET(_context, params) {
    const { data } = await Axios.get("/speaker", { params });

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async SHOW(_context, id) {
    const { data } = await Axios.get(`/speaker/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async STORE(_context, values) {
    const { data } = await Axios.post(`/speaker`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPDATE(_context, values) {
    const { data } = await Axios.put(`/speaker/${values.id}`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DESTROY(_context, id) {
    const { data } = await Axios.delete(`/speaker/${id}`);

    if (!data.success) {
      throw data.message;
    }
  }
};

export default {
  namespaced: true,
  actions
};
