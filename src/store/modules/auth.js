import Axios from "axios";

const user = localStorage.getItem("user");
const token = localStorage.getItem("token");

if (token) {
  Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

const state = () => ({
  user: (user && JSON.parse(user)) || null,
  token: token || null
});

const mutations = {
  SET_USER(state, user) {
    if (user) {
      localStorage.setItem("user", JSON.stringify(user));
      state.user = user;
    } else {
      localStorage.removeItem("user");
      state.user = null;
    }
  },
  SET_TOKEN(state, token) {
    if (token) {
      Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      localStorage.setItem("token", token);
      state.token = token;
    } else {
      localStorage.removeItem("token");
      state.token = null;
    }
  }
};

const actions = {
  async DO_LOGIN({ commit }, credentials) {
    const { data } = await Axios.post("/auth/login", credentials);

    if (!data.success) {
      throw data.message;
    }

    commit("SET_TOKEN", data.data);

    return data.data;
  },
  async DO_LOGOUT({ commit }) {
    commit("SET_TOKEN", null);
    commit("SET_USER", null);
  },
  async GET_USER({ commit }) {
    const { data } = await Axios.get("/auth/user");

    if (!data.success) {
      throw data.message;
    }

    commit("SET_USER", data.data);

    return data.data;
  },
  async UPDATE_USER(_context, values) {
    const { data } = await Axios.put("/auth/user", values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DO_REGISTER(values) {
    const { data } = await Axios.post("/auth/register", values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
