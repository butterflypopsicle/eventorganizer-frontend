import Axios from "axios";

const actions = {
  async GET(_context, params) {
    const { data } = await Axios.get("/eo", { params });

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async SHOW(_context, id) {
    const { data } = await Axios.get(`/eo/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async STORE(_context, values) {
    const { data } = await Axios.post(`/eo`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPDATE(_context, values) {
    const { data } = await Axios.put(`/eo/${values.id}`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DESTROY(_context, id) {
    const { data } = await Axios.delete(`/eo/${id}`);

    if (!data.success) {
      throw data.message;
    }
  }
};

export default {
  namespaced: true,
  actions
};
