import Axios from "axios";

const actions = {
  async GET() {
    const { data } = await Axios.post("/schedule");

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
};

export default {
  namespaced: true,
  actions,
};
