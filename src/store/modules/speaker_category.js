import Axios from "axios";

const actions = {
  async GET(_context, params) {
    const { data } = await Axios.get("/speakercategory", { params });

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async SHOW(_context, id) {
    const { data } = await Axios.get(`/speakercategory/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async STORE(_context, values) {
    const { data } = await Axios.post(`/speakercategory`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPDATE(_context, values) {
    const { data } = await Axios.put(`/speakercategory/${values.id}`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DESTROY(_context, id) {
    const { data } = await Axios.delete(`/speakercategory/${id}`);

    if (!data.success) {
      throw data.message;
    }
  }
};

export default {
  namespaced: true,
  actions
};
