import axios from "axios";

const actions = {
  async GET(_context, params) {
    const { data } = await axios.get("/eventtenant", { params });

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async SHOW(_context, id) {
    const { data } = await axios.get(`/eventtenant/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async STORE(_context, values) {
    const { data } = await axios.post(`/eventtenant`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPDATE(_context, values) {
    const { data } = await axios.put(`/eventtenant/${values.id}`, values);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async GET_REGISTRATION(_context, values) {
    const { data } = await axios.get(`/event/registration/${values}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async UPLOAD_PROOF(_context, values) {
    const { data } = await axios.post(
      `/eventtenant/upload/${values.id}`,
      values.data,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  },
  async DESTROY(_context, id) {
    const { data } = await axios.delete(`/eventtenant/${id}`);

    if (!data.success) {
      throw data.message;
    }
  },
  async GET_PENDING(_context, id) {
    const { data } = await axios.get(`/event/pending/${id}`);

    if (!data.success) {
      throw data.message;
    }

    return data.data;
  }
};

export default {
  namespaced: true,
  actions
};
