import Vue from "vue";
import Vuex from "vuex";

import auth from "./modules/auth";
import event from "./modules/event";
import event_speaker from "./modules/event_speaker";
import event_tenant from "./modules/event_tenant";
import institution from "./modules/institution";
import schedule from "./modules/schedule";
import speaker_category from "./modules/speaker_category";
import speaker from "./modules/speaker";
import eo from "./modules/eo";
import tenant from "./modules/tenant";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    institution,
    event,
    event_speaker,
    event_tenant,
    speaker_category,
    schedule,
    speaker,
    eo,
    tenant
  }
});

export default store;
