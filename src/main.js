import Vue from "vue";
import Axios from "axios";

import App from "./App.vue";

import router from "./router";
import store from "./store";

import "./registerServiceWorker";

Vue.config.productionTip = false;

Axios.defaults.baseURL = process.env.VUE_APP_API_URL;
Vue.prototype.$http = Axios;

import ImageUploader from "vue-image-upload-resize";
Vue.use(ImageUploader);

import VueCroppie from "vue-croppie";
import "croppie/croppie.css";
Vue.use(VueCroppie);

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "./assets/css/style.min.css";

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

new Vue({
  router,
  render: (h) => h(App),
  store,
}).$mount("#app");
