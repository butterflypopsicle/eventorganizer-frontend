import dayjs from "dayjs";

export default {
  methods: {
    dayjs(date) {
      return dayjs(date);
    },
  },
};
