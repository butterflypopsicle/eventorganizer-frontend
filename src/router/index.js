import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Auth from "../views/Auth.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "*",
    redirect: { name: "Dashboard" }
  },
  {
    path: "/auth",
    name: "Auth",
    component: Auth,
    children: [
      {
        name: "Login",
        path: "login",
        component: () => import("../views/pages/Auth/Login.vue")
      },
      {
        name: "Register",
        path: "register",
        component: () => import("../views/pages/Auth/Register.vue")
      },
      {
        name: "RegisterEO",
        path: "register-eo",
        component: () => import("../views/pages/Auth/RegisterEO.vue")
      },
      {
        name: "RegisterTenant",
        path: "register-tenant",
        component: () => import("../views/pages/Auth/RegisterTenant.vue")
      },
      {
        name: "RegisterSpeaker",
        path: "register-speaker",
        component: () => import("../views/pages/Auth/RegisterSpeaker.vue")
      },
      {
        path: "",
        redirect: { name: "Login" }
      }
    ]
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      {
        name: "Dashboard",
        path: "dashboard",
        component: () => import("../views/pages/Dashboard.vue")
      },
      {
        name: "Event",
        path: "event",
        component: () => import("../views/pages/Event/Event.vue")
      },
      {
        name: "Event Paginate",
        path: "event/page/:page",
        component: () => import("../views/pages/Event/Event.vue")
      },
      {
        name: "CreateEvent",
        path: "event/create",
        component: () => import("../views/pages/Event/FormEvent.vue")
      },
      {
        name: "ShowEvent",
        path: "event/:id",
        component: () => import("../views/pages/Event/DetailEvent.vue")
      },
      {
        name: "EditEvent",
        path: "event/:id/edit",
        component: () => import("../views/pages/Event/FormEvent.vue")
      },
      {
        name: "SpeakerCategory",
        path: "speaker-category",
        component: () =>
          import("../views/pages/Speaker/Category/KategoriPengisiAcara.vue")
      },
      {
        name: "SpeakerCategory Paginate",
        path: "speaker-category/page/:page",
        component: () =>
          import("../views/pages/Speaker/Category/KategoriPengisiAcara.vue")
      },
      {
        name: "ShowCategory",
        path: "speaker-category/show/:id",
        component: () =>
          import("../views/pages/Speaker/Category/FormKategoriPengisiAcara.vue")
      },
      {
        name: "CreateCategory",
        path: "speaker-category/create",
        component: () =>
          import("../views/pages/Speaker/Category/FormKategoriPengisiAcara.vue")
      },
      {
        name: "Speaker",
        path: "speaker",
        component: () => import("../views/pages/Speaker/Index.vue")
      },
      {
        name: "ShowSpeaker",
        path: "speaker/show/:id",
        component: () => import("../views/pages/Speaker/Form.vue")
      },
      {
        name: "CreateSpeaker",
        path: "speaker/create",
        component: () => import("../views/pages/Speaker/Form.vue")
      },
      {
        name: "SpeakerProfile",
        path: "speaker-profile",
        component: () =>
          import("../views/pages/Speaker/DataSpeaker/ProfilPengisiAcara.vue")
      },

      {
        name: "Tenant",
        path: "tenant",
        component: () => import("../views/pages/Tenant/Tenant.vue")
      },
      {
        name: "Tenant Paginate",
        path: "tenant/page/:page",
        component: () => import("../views/pages/Tenant/Tenant.vue")
      },
      {
        name: "Tenant",
        path: "tenant/show/:id",
        component: () => import("../views/pages/Tenant/FormTenant.vue")
      },
      {
        name: "CreateTenant",
        path: "tenant/create",
        component: () => import("../views/pages/Tenant/FormTenant.vue")
      },
      {
        name: "TenantProfile",
        path: "tenant-profile",
        component: () => import("../views/pages/Tenant/ProfilTenant.vue")
      },

      {
        name: "Profile",
        path: "pfile_eo",
        component: () => import("../views/pages/EO/DataEO/ProfileEO.vue")
      },
      {
        name: "Event Organizer",
        path: "eo",
        component: () => import("../views/pages/EO/DataEO/EO.vue")
      },
      {
        name: "EO Paginate",
        path: "eo/d_eo/page/:page",
        component: () => import("../views/pages/EO/DataEO/EO.vue")
      },
      {
        name: "ShowEO",
        path: "eo/d_eo/show/:id",
        component: () => import("../views/pages/EO/DataEO/FormEO.vue")
      },
      {
        name: "CreateEO",
        path: "eo/d_eo/create",
        component: () => import("../views/pages/EO/DataEO/FormEO.vue")
      },
      {
        name: "Lembaga",
        path: "lembaga",
        component: () => import("../views/pages/EO/Lembaga/Lembaga.vue")
      },
      {
        name: "Lembaga Paginate",
        path: "eo/lembaga/page/:page",
        component: () => import("../views/pages/EO/Lembaga/Lembaga.vue")
      },
      {
        name: "ShowLembaga",
        path: "eo/lembaga/show/:id",
        component: () => import("../views/pages/EO/Lembaga/FormLembaga.vue")
      },
      {
        name: "CreateLembaga",
        path: "eo/lembaga/create",
        component: () => import("../views/pages/EO/Lembaga/FormLembaga.vue")
      },

      {
        name: "Profile",
        path: "profile",
        component: () => import("../views/pages/Profile.vue")
      },

      {
        name: "About",
        path: "about",
        component: () => import("../views/pages/About.vue")
      },
      {
        path: "",
        redirect: { name: "Dashboard" }
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
